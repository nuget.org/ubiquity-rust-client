# BlockIdentifierPage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | Option<**i32**> | Number of items in block identifiers | [optional]
**items** | Option<[**Vec<crate::models::BlockIdentifier>**](block_identifier.md)> |  | [optional]
**continuation** | Option<**i32**> | Token to get the next page | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


