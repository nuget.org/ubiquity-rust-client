# TxConfirmation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**current_height** | Option<**i64**> | Current Block Number | [optional]
**tx_id** | Option<**String**> | Transaction hash | [optional]
**confirmations** | Option<**i64**> | Total transaction confirmations | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


