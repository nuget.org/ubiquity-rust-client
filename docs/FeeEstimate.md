# FeeEstimate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**most_recent_block** | Option<**i32**> | Most recent block | [optional]
**estimated_fees** | Option<[**crate::models::FeeEstimateEstimatedFees**](fee_estimate_estimated_fees.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


